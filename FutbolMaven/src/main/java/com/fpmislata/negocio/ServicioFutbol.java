/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fpmislata.negocio;

import com.fpmislata.datos.FutbolHibernate;

import com.fpmislata.datos.IFutbolDAO;
import com.fpmislata.entidades.Equipo;
import com.fpmislata.entidades.Jugador;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author rafael
 */
// Las clases de Negocio como esta suponen una abstracción más, las clases de
// datos
// se encargan de introducir u obtener información del sistema de almacenamiento
// pero ya está
// pero ¿ Que ocurre si quiero hacer algún tipo de control o procesamiento sobre
// esa información?
// Por ejemplo quiero calcular la media de presupuesto de los equipos o quiero
// comprobar
// que antes de insertar un jugador tiene asignado un equipo. ¿Dónde meto ese
// código?, está claro
// que en las clases de arriba (La interfaz de consola en este caso) no puede
// ir, si mañana decido
// hacer la aplicación Web tendría que volver a escribir ese código. En las
// clases de Datos tampoco
// ya que insertar jugador debe de hacer exactamente lo que dice, si decido
// reciclar mi clase en otro
// programa (ya sea yo u otro programador) no hay nada peor que algo tenga un
// comportamiento oculto,
// como norma general un método debe hacer una sola cosa, a excepción de las
// clases de servicio que pueden
// realizar varias comprobaciones o cálculos según quiera yo que se comporte mi
// aplicación. A menudo las clases de servicio
// solamente llaman a las clases de datos y punto, pero no siempre.
// En el MVC las clases de servicio de la capa de negocio deben de ser las
// únicas accesibles a las clases
// Controlador
public class ServicioFutbol implements IServicioFutbol {
    private IFutbolDAO idao = null;
   

    
    

    public ServicioFutbol() throws Exception{
        
         idao = new FutbolHibernate();
    }

   

    @Override
    public void nuevoJugador(int id, String nombre, String fechaNac,
            int idEquipo) throws Exception {
		// El curso pasado en algunos ejemplos pasabamos desde la interfaz
        // gráfica la clase Jugador
        // ya formada, no es que estuviese mal, pero así en el nivel de arriba
        // no se tiene que proecupar de crear la
        // clase jugador, pasa datos simples y se olvida, ya se encargará el
        // nivel de abajo.
        // siguiendo con la misma filosofia pasamos la fecha como String, y así
        // las comprobaciones
        // las hacemos todas aquí, el nivel de arriba se encarga exclusivamente
        // de lo que le toca, si es la clase
        // controlador pues se encarga solo de redirigir a la vista que toca, si
        // es todo integrado en la interfaz de consoloa
        // como en este caso pues se encarga de todas sus funciones.
        Jugador j = new Jugador();
        if (id <= 0) {
            throw new ServicioFutbolException("El identificador debe ser mayor que 0");
        }
        if (idEquipo <= 0) {
            throw new ServicioFutbolException(
                    "El identificador de Equipo debe ser mayor que 0");
        }
        j.setId(id);
        j.setNombre(nombre);
        // ¿Os acordais de esto?
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date d = null;
        try {
            d = sdf.parse(fechaNac);
        } catch (ParseException ex) { // Capturamos la excepción y ..
            // la lanzamos para informar al nivel de arriba
            throw new ServicioFutbolException("El formato de fecha no es válido");
        }
        j.setFechaNacimiento(d);

        // Vamos a comprobar que no existe el jugador
        Jugador jPrueba = idao.obtenerJugador(j.getId());
        if (jPrueba != null) {
            throw new ServicioFutbolException("Ya existe un jugador con ese Id");
        }

        Equipo e = idao.obtenerEquipo(idEquipo);
		// En esta aplicación suponemos que todo jugador debe pertenecer a un
        // equipo
        if (e == null) {
            throw new ServicioFutbolException("No existe ningún equipo con ese id");
        }
        j.setEquipo(e);
        try {
            idao.nuevoJugador(j);
        } catch (Exception e2) {
            throw new Exception("Ha habido un problema al guardar el jugador: "
                    + e2.getCause());
        }

    }

    @Override
    public void nuevoEquipo(int id, String nombre, double presupuesto)
            throws Exception {
		// Comprobamos que el equipo no existe

        if (id <= 0) {
            throw new ServicioFutbolException(
                    "El identificador debe ser mayor que 0");
        }
        Equipo e = idao.obtenerEquipo(id);
        if (e != null) {
            throw new ServicioFutbolException("Ya existe un equipo con ese Id");
        }
        e = new Equipo();

        e.setId(id);
        e.setNombre(nombre);
        e.setPresupuesto(presupuesto);
        try {
            idao.nuevoEquipo(e);
        } catch (Exception e2) {
            throw new Exception("Ha habido un problema al guardar el equipo: "
                    + e2.getLocalizedMessage());
        }
    }

    @Override
    public List<Jugador> obtenerTodosJugadores() throws Exception {
        try {
            List<Jugador> jugadores = idao.obtenerTodosJugadores();
            // si no hay nada devuelve una lista vacia
            if (jugadores == null) {
                jugadores = new ArrayList<Jugador>();
            }
            return jugadores;
        } catch (Exception e) {
            throw new Exception(
                    "Ha habido un problema al obtener los jugadores: "
                    + e.getLocalizedMessage());
        }
    }

    @Override
    public List<Equipo> obtenerTodosEquipos() throws Exception {
        try {
            List<Equipo> equipos = idao.obtenerTodosEquipos();
            // si no hay nada devuelve una lista vacia
            if (equipos == null) {
                equipos = new ArrayList<Equipo>();
            }
            return equipos;
        } catch (Exception e) {
            throw new Exception(
                    "Ha habido un problema al obtener los equipos: "
                    + e.getLocalizedMessage());
        }
    }

    @Override
    public void eliminarJugador(int idJugador) throws Exception {
        try {
            idao.eliminarJugador(idJugador);
        } catch (Exception e) {
            throw new Exception(
                    "Ha habido un problema al eliminar el jugador: "
                    + e.getLocalizedMessage());
        }
    }

    @Override
    public void finalizar() throws Exception {
        idao.finalizar();
        
    }
}
