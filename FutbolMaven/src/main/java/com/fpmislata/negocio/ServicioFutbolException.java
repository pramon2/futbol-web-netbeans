package com.fpmislata.negocio;

public class ServicioFutbolException extends Exception {
	
	
	private static final long serialVersionUID = 1L;
	

	public ServicioFutbolException (String mensaje)
    {
        super(mensaje);
    }

}
