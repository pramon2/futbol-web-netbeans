/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fpmislata.negocio;

import com.fpmislata.entidades.Equipo;
import com.fpmislata.entidades.Jugador;
import java.util.List;

/**
 *
 * @author Pablo
 */
public interface IServicioFutbol {

    void eliminarJugador(int idJugador) throws Exception;

    void finalizar() throws Exception;

    void nuevoEquipo(int id, String nombre, double presupuesto) throws Exception;

    void nuevoJugador(int id, String nombre, String fechaNac, int idEquipo) throws Exception;

    List<Equipo> obtenerTodosEquipos() throws Exception;

    List<Jugador> obtenerTodosJugadores() throws Exception;
    
}
