/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fpmislata.negocio;

/**
 *
 * @author Pablo
 */
public class FactoriaServicios {
  
 
       private  static FactoriaServicios me;
 
       private  FactoriaServicios() { }
 
       public static FactoriaServicios getInstance() {
          if (me == null) {
             me = new FactoriaServicios();
          }
          return me;
       }
 
       public IServicioFutbol getServicioFutbol() throws Exception {
          return new ServicioFutbol();
       }
 
}
