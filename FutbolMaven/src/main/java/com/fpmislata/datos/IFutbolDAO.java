/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fpmislata.datos;

import com.fpmislata.entidades.Equipo;
import com.fpmislata.entidades.Jugador;

import java.util.List;

/**
 *
 * @author rafael
 */
public interface IFutbolDAO {
    public void nuevoJugador(Jugador j) throws Exception;
    public void nuevoEquipo(Equipo e) throws Exception;
    public void eliminarJugador(int id) throws Exception;
    public Jugador obtenerJugador(int idJugador) throws Exception;
    public Equipo obtenerEquipo(int idEquipo) throws Exception;
    public List<Jugador> obtenerTodosJugadores() throws Exception;
    public List<Equipo> obtenerTodosEquipos() throws Exception;
    public void finalizar() throws Exception;
    
    
}
