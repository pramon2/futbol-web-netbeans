package com.fpmislata.datos;

import java.util.List;


import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import com.fpmislata.entidades.Equipo;
import com.fpmislata.entidades.Jugador;


public class FutbolHibernate implements IFutbolDAO {
	

	public FutbolHibernate() {
		
		
	}

	
	public void nuevoJugador(Jugador j) throws Exception {
                 Session sesion =HibernateUtil.getSessionFactory().openSession();
		
		// Iniciamos una transacci�n
		Transaction tx = sesion.beginTransaction();
		
		// Hacemos los cambios
		sesion.save(j);
		
		// Cerramos la transacci�n
		tx.commit();
		// Cerramos la sesi�n
		sesion.close();

	}

	
	public void nuevoEquipo(Equipo e) throws Exception {
		
		 Session sesion =HibernateUtil.getSessionFactory().openSession();
			
			// Iniciamos una transacci�n
			Transaction tx = sesion.beginTransaction();
			
			// Hacemos los cambios
			sesion.save(e);
			
			// Cerramos la transacci�n
			tx.commit();
			
			// Cerramos la sesi�n
			sesion.close();

	}

	
	public void eliminarJugador(int id) throws Exception {
		   Jugador j=this.obtenerJugador(id);
		   Session sesion =HibernateUtil.getSessionFactory().openSession();
			
			// Iniciamos una transacci�n
			Transaction tx = sesion.beginTransaction();
			
			// Hacemos los cambios
			sesion.delete(j);
			
			// Cerramos la transacci�n
			tx.commit();
			
			// Cerramos la sesi�n
			sesion.close();

	}

	
	public Jugador obtenerJugador(int idJugador) throws Exception {
		Session sesion = HibernateUtil.getSessionFactory().openSession();
		
		// Variable que almacena la lista a devolver
		Jugador j= new Jugador();
		/*OPCION 1 CON UNA CONSULTA HQL
		// Hacemos la consulta
		String consulta = "FROM Jugador AS J WHERE J.id = :idJugador";
		Query q = sesion.createQuery(consulta).setInteger("idJugador", idJugador);
		@SuppressWarnings("unchecked")
		List<Jugador>lista = q.list();
		if(lista.size()==0){
			j=null;
		}
		else{
			j=lista.get(0);
		}
		// Cerramos la sesi�n
		sesion.close();
		
		// Devolvemos el resultado
		return j;
		*/
		//OPCION 2 USANDO GET SOLO SIRVE PARA BUSCAR POR EL ID
		j=(Jugador) sesion.get(Jugador.class, idJugador);
		sesion.close();
		return j;
		
	}

	
	
	public Equipo obtenerEquipo(int idEquipo) throws Exception {
		Session sesion = HibernateUtil.getSessionFactory().openSession();
		
		// Variable que almacena la lista a devolver
		Equipo e= new Equipo();
		
		/*OPCION 1 CON UNA CONSLUATA HQL
		// Hacemos la consulta
		String consulta = "FROM Equipo AS E WHERE E.id = :idEquipo";
		Query q = sesion.createQuery(consulta).setInteger("idEquipo", idEquipo);
		List<Equipo>lista = q.list();
		if(lista.size()==0){
			e=null;
		}
		else{
			e=lista.get(0);
		}
		
		// Cerramos la sesi�n
		sesion.close();
		
		// Devolvemos el resultado
		return e;
		*/
		
		//OPCION 2
		e=(Equipo) sesion.get(Equipo.class, idEquipo);
		sesion.close();
		return e;
	}

	@SuppressWarnings("unchecked")
	
	public List<Jugador> obtenerTodosJugadores() throws Exception {
		Session sesion = HibernateUtil.getSessionFactory().openSession();
		
		// Variable que almacena la lista a devolver
		List<Jugador> lista;
		
		// Hacemos la consulta
		Query q = sesion.createQuery("FROM Jugador");
		lista = q.list();
		for(Jugador j :lista){
			Hibernate.initialize(j.getEquipo());
		}
		
		// Cerramos la sesi�n
		sesion.close();
		
		// Devolvemos el resultado
		return lista;
	}

	@SuppressWarnings("unchecked")
	
	public List<Equipo> obtenerTodosEquipos() throws Exception {
		Session sesion = HibernateUtil.getSessionFactory().openSession();
		
		// Variable que almacena la lista a devolver
		List<Equipo> lista;
		
		// Hacemos la consulta
		Query q = sesion.createQuery("FROM Equipo");
		lista = q.list();
		
		// Cerramos la sesi�n
		sesion.close();
		
		// Devolvemos el resultado
		return lista;
	}


	public void finalizar() throws Exception {
		HibernateUtil.finalizarSessionFactory();

	}

}
