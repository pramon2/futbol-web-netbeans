/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fpmislata.datos;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 *
 * @author Pablo
 */
public class HibernateUtil {

    private HibernateUtil() {

    }

    private static SessionFactory sesionFactory = null;

    public static  SessionFactory getSessionFactory() throws Exception {
        try {

            if (sesionFactory == null) {
                ServiceRegistry serviceRegistry;
		Configuration configuration = new Configuration();
	        configuration.configure();
	        serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();        
	        sesionFactory = configuration.buildSessionFactory(serviceRegistry);
            }
        } catch (HibernateException e) {
            // TODO Auto-generated catch block  
           throw new Exception("No ha sido posible inicilizar la sesion factory");
        }
        return sesionFactory;
    }
    public static void finalizarSessionFactory(){
        sesionFactory = null;
    }

   

}
