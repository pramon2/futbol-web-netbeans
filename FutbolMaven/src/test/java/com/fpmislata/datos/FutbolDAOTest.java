package com.fpmislata.datos;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.fpmislata.entidades.Equipo;
import com.fpmislata.entidades.Jugador;

public abstract class FutbolDAOTest {
	
	protected IFutbolDAO idao=null;

	@Before
	public abstract void setUp() throws Exception;
	@After
	public abstract void tearDown() throws Exception;
	
	
	@Test
	public void testObtenerTodosEquiposVacio() {
		try{
			List<Equipo> equipos= idao.obtenerTodosEquipos();
			if(equipos.size()!=0){
				fail("la lista no está inicialmente vacia");
			}
			
			
			}catch(Exception e){
				fail("TEST NO SUPERADO: No se ha podido obtener la lista vacia  de equipos al producirse una excepcion: "+e.getLocalizedMessage());
			}
	}
	
	@Test
	public void testNuevoEquipo() {
		try{
		Equipo e1=new Equipo();
		e1.setId(1);
		e1.setNombre("Real Madrid");
		e1.setPresupuesto(10000000);
		
		Equipo e2=new Equipo();
		e2.setId(2);
		e2.setNombre("FC Barcelona");
		e1.setPresupuesto(20000000);
		
		idao.nuevoEquipo(e1);
		idao.nuevoEquipo(e2);
		
		
		}catch(Exception e){
			fail("TEST NO SUPERADO: No se han insertado los equipos al producirse una excepcion: "+e.getLocalizedMessage());
		}
	}
	@Test
	public void testObtenerTodosEquiposLLeno() {
		try{
			Equipo e1=new Equipo();
			e1.setId(1);
			e1.setNombre("Real Madrid");
			e1.setPresupuesto(10000000);
			
			Equipo e2=new Equipo();
			e2.setId(2);
			e2.setNombre("FC Barcelona");
			e1.setPresupuesto(20000000);
			
			idao.nuevoEquipo(e1);
			idao.nuevoEquipo(e2);
			List<Equipo> equipos= idao.obtenerTodosEquipos();
			if(equipos.size()!=2){
				fail("la lista no está inicialmente con dos equipos");
			}
			for(Equipo e: equipos) {
				assertNotNull(e.getId());
				assertNotNull(e.getNombre());
				assertNotNull(e.getPresupuesto());
			}
			
			}catch(Exception e){
				fail("TEST NO SUPERADO: No se ha podido obtener la lista   de equipos al producirse una excepcion: "+e.getLocalizedMessage());
			}
	}
	
	@Test
	public void testObtenerTodosJugadoresVacio() {
		try{
			List<Jugador> jugadores= idao.obtenerTodosJugadores();
			if(jugadores.size()!=0){
				fail("la lista de jugadores no está inicialmente vacia");
			}
			
			
			}catch(Exception e){
				fail("TEST NO SUPERADO: No se ha podido obtener la lista vacia  de jugadores al producirse una excepcion: "+e.getLocalizedMessage());
			}
	}

	@Test
	public void testNuevoJugador() {
		try{
			Equipo e1=new Equipo();
			e1.setId(1);
			e1.setNombre("Real Madrid");
			e1.setPresupuesto(10000000);
			
			idao.nuevoEquipo(e1);
			SimpleDateFormat sdf= new SimpleDateFormat("dd/MM/yyyy");
			

			Jugador j1= new Jugador();
			Date d1=sdf.parse("2/1/1998");
			j1.setId(1);
			j1.setNombre("Iker Casillas");
			j1.setFechaNacimiento(d1);
			j1.setEquipo(e1);
			
			
			
		    Jugador j2= new Jugador();
			Date d2=sdf.parse("2/3/1990");
			j2.setId(2);
			j2.setNombre("Gareth Bale");
			j2.setFechaNacimiento(d2);
			j2.setEquipo(e1);
			
			
			
		 	Jugador j3= new Jugador();
			Date d3=sdf.parse("4/10/1998");
			j3.setId(3);
			j3.setNombre("Cristiano Romaldo");
			j3.setFechaNacimiento(d3);
			j3.setEquipo(e1);
			
			idao.nuevoJugador(j1);
			idao.nuevoJugador(j2);
			idao.nuevoJugador(j3);
			
			
			}catch(Exception e){
				fail("TEST NO SUPERADO: No se han insertado los jugadores al producirse una excepcion: "+e.getLocalizedMessage());
			}
	}

	

	@Test
	public void testObtenerJugador() {
		try{
			
			Equipo e1=new Equipo();
			e1.setId(1);
			e1.setNombre("Real Madrid");
			e1.setPresupuesto(10000000);
			
			idao.nuevoEquipo(e1);
			SimpleDateFormat sdf= new SimpleDateFormat("dd/MM/yyyy");
			

			Jugador j1= new Jugador();
			Date d1=sdf.parse("2/1/1998");
			j1.setId(1);
			j1.setNombre("Iker Casillas");
			j1.setFechaNacimiento(d1);
			j1.setEquipo(e1);
			idao.nuevoJugador(j1);
			
			Jugador ju2=idao.obtenerJugador(1);
			if(ju2.getNombre()==null){
				fail("No se ha obtenido el jugador apropiadamente, el jugador no tiene nombre");
			}
			if(ju2.getFechaNacimiento()==null){
				fail("No se ha obtenido el jugador apropiadamente, el jugador no tiene fecha de nacimiento");
			}
			if(ju2.getEquipo()==null){
				fail("No se ha obtenido el jugador apropiadamente, el jugador no tiene equipo");
			}
			
			if(!(ju2.getId()==1)){
				fail("No se ha obtenido el jugador apropiadamente, el id no es correcto");
			}
			if(!(ju2.getNombre().equals("Iker Casillas"))){
				fail("No se ha obtenido el jugador apropiadamente, el jugador no tiene el nombre correcto");
			}
			if(!(ju2.getFechaNacimiento().getTime()==d1.getTime())){
				fail("No se ha obtenido el jugador apropiadamente, el jugador no tiene la fecha de nacimiento correcta");
			}
			if(!(ju2.getEquipo().getId()==e1.getId())){
				fail("No se ha obtenido el jugador apropiadamente, el jugador no tiene el equipo correcto");
			}
			
			
			}catch(Exception e){
				fail("TEST NO SUPERADO: No se ha podido obtener la lista de jugadores al producirse una excepcion: "+e.getLocalizedMessage());
			}
	}

	@Test
	public void testObtenerEquipo() {
		try{
			
			Equipo e1=new Equipo();
			e1.setId(1);
			e1.setNombre("Real Madrid");
			e1.setPresupuesto(10000000);
			
			idao.nuevoEquipo(e1);
			Equipo e2=idao.obtenerEquipo(1);
			
			
			if(e2.getNombre()==null){
				fail("No se ha obtenido el equipo apropiadamente, el equipo no tiene nombre");
			}
			
			
			if(!(e2.getId()==1)){
				fail("No se ha obtenido el equipo apropiadamente, el id no es correcto");
			}
			if(!(e2.getNombre().equals("Real Madrid"))){
				fail("No se ha obtenido el equipo apropiadamente, el equipo no tiene el nombre correcto");
			}
			if(!(e2.getPresupuesto()==10000000)){
				fail("No se ha obtenido el equipo apropiadamente, el equipo no tiene el presupuesto correcto");
			}
			
			
			
			}catch(Exception e){
				fail("TEST NO SUPERADO: No se ha podido obtener la lista de equipos al producirse una excepcion: "+e.getLocalizedMessage());
			}
	}

	@Test
	public void testObtenerTodosJugadoresLLeno() {
		try{
		Equipo e1=new Equipo();
		e1.setId(1);
		e1.setNombre("Real Madrid");
		e1.setPresupuesto(10000000);
		
		idao.nuevoEquipo(e1);
		SimpleDateFormat sdf= new SimpleDateFormat("dd/MM/yyyy");
		

		Jugador j1= new Jugador();
		Date d1=sdf.parse("2/1/1998");
		j1.setId(1);
		j1.setNombre("Iker Casillas");
		j1.setFechaNacimiento(d1);
		j1.setEquipo(e1);
		
		
		
	    Jugador j2= new Jugador();
		Date d2=sdf.parse("2/3/1990");
		j2.setId(2);
		j2.setNombre("Gareth Bale");
		j2.setFechaNacimiento(d2);
		j2.setEquipo(e1);
		
		
		
	 	Jugador j3= new Jugador();
		Date d3=sdf.parse("4/10/1998");
		j3.setId(3);
		j3.setNombre("Cristiano Romaldo");
		j3.setFechaNacimiento(d3);
		j3.setEquipo(e1);
		
		idao.nuevoJugador(j1);
		idao.nuevoJugador(j2);
		idao.nuevoJugador(j3);
		
		List<Jugador> jugadores= idao.obtenerTodosJugadores();
		for(Jugador j :jugadores){
			assertNotNull(j.getId());
			assertNotNull(j.getNombre());
			assertNotNull(j.getFechaNacimiento());
			assertNotNull(j.getEquipo());
		}
		if(jugadores.size()!=3){
			fail("TEST NO SUPERADO: La lista de jugadores es incorrecta");
		}
		
		
		}catch(Exception e){
			fail("TEST NO SUPERADO: No se ha podido obtener la lista de jugadores al producirse una excepcion: "+e.getLocalizedMessage());
		}
	}

	

	@Test
	public void testEliminarJugador() {
		try{
		Equipo e1=new Equipo();
		e1.setId(1);
		e1.setNombre("Real Madrid");
		e1.setPresupuesto(10000000);
		
		idao.nuevoEquipo(e1);
		SimpleDateFormat sdf= new SimpleDateFormat("dd/MM/yyyy");
		

		Jugador j1= new Jugador();
		Date d1=sdf.parse("2/1/1998");
		j1.setId(1);
		j1.setNombre("Iker Casillas");
		j1.setFechaNacimiento(d1);
		j1.setEquipo(e1);
		
		
		
	    Jugador j2= new Jugador();
		Date d2=sdf.parse("2/3/1990");
		j2.setId(2);
		j2.setNombre("Gareth Bale");
		j2.setFechaNacimiento(d2);
		j2.setEquipo(e1);
		
		
		
	 	Jugador j3= new Jugador();
		Date d3=sdf.parse("4/10/1998");
		j3.setId(3);
		j3.setNombre("Cristiano Romaldo");
		j3.setFechaNacimiento(d3);
		j3.setEquipo(e1);
		
		idao.nuevoJugador(j1);
		idao.nuevoJugador(j2);
		idao.nuevoJugador(j3);
		
		List<Jugador> jugadores=idao.obtenerTodosJugadores();
		for(Jugador j :jugadores){
			assertNotNull(j.getId());
			assertNotNull(j.getNombre());
			assertNotNull(j.getFechaNacimiento());
			assertNotNull(j.getEquipo());
		}
		
		if(jugadores.size()!=3){
			fail("TEST NO SUPERADO: La lista de jugadores es incorrecta");
		}
		
		idao.eliminarJugador(2);
		jugadores=idao.obtenerTodosJugadores();
		if(jugadores.size()==3){
			fail("TEST NO SUPERADO: No se ha eliminado el jugador");
		}
		if(idao.obtenerJugador(2)!=null){
			fail("TEST NO SUPERADO: No se ha eliminado el jugador Seleccionado");
		}
		
		
		
		}catch(Exception e){
			fail("TEST NO SUPERADO: No se ha podido obtener la lista de jugadores al producirse una excepcion: "+e.getLocalizedMessage());
		}
	}

	

}
