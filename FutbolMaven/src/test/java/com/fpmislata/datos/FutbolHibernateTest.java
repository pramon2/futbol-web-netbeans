 package com.fpmislata.datos;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class FutbolHibernateTest extends FutbolDAOTest {
	static String bd = "futbolHiberTest";
	static String login = "root";
	static String password = "root";
	static String url = "jdbc:mysql://localhost/" + bd;

	static String deleteJugadores = "DELETE FROM jugadores;";
	static String deleteEquipos = "DELETE FROM equipos;";

	
	
	@Override
	public void tearDown() throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		try {
			idao.finalizar();

			conn = DriverManager.getConnection(url, login, password);

			ps = conn.prepareStatement(deleteJugadores);
			ps.executeUpdate();
			ps2 = conn.prepareStatement(deleteEquipos);
			ps2.executeUpdate();

		} catch (Exception ex) {

			System.out.println("Ha habido un problema al iniciar los test"
					+ ex.getLocalizedMessage());
		} finally {
			try {
				ps.close();
				ps2.close();
				conn.close();
			} catch (SQLException ex) {
				throw new Exception("Error al cerrar la base de datos");
			}

		}

	}

	@Override
	public void setUp() throws Exception {
		try{
			
	   
	    	idao= new FutbolHibernate();
	    	
	    	} catch (Exception ex) {
	    	
	       	 System.out.println("No ha ssido posible iniciar la sesion"+ex.getLocalizedMessage());
	        }

	}

}
