package com.fpmislata.negocio;

import com.fpmislata.datos.FutbolHibernate;
import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import java.util.List;
import java.util.Properties;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fpmislata.entidades.Equipo;
import com.fpmislata.entidades.Jugador;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ServicioFutbolTest {
	private  ServicioFutbol sf = null;

	static String bd = "futbolHiberTest";
	static String login = "root";
	static String password = "root";
	static String url = "jdbc:mysql://localhost/" + bd;

	static String deleteJugadores = "DELETE FROM jugadores;";
	static String deleteEquipos = "DELETE FROM equipos;";

	
	
        @After
	public void tearDown() throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		try {
			sf.finalizar();

			conn = DriverManager.getConnection(url, login, password);

			ps = conn.prepareStatement(deleteJugadores);
			ps.executeUpdate();
			ps2 = conn.prepareStatement(deleteEquipos);
			ps2.executeUpdate();

		} catch (Exception ex) {

			System.out.println("Ha habido un problema al iniciar los test"
					+ ex.getLocalizedMessage());
		} finally {
			try {
				ps.close();
				ps2.close();
				conn.close();
			} catch (SQLException ex) {
				throw new Exception("Error al cerrar la base de datos");
			}

		}

	}
        @Before
	public void setUp() throws Exception {
		try{
			
	   
	    	sf= new ServicioFutbol();
	    	
	    	} catch (Exception ex) {
	    	
	       	 System.out.println("No ha ssido posible iniciar la sesion"+ex.getLocalizedMessage());
	        }

	}

	@Test
	public void testNuevoJugadorOK() {
		try {
                        sf.nuevoEquipo(1, "Real Madrid", 1000);
			sf.nuevoJugador(3, "Gareth Bale", "2/3/1980", 1);
			List<Jugador> jugadores = sf.obtenerTodosJugadores();
			if (jugadores.size() != 1) {
				fail("No se ha insertado el jugador");
			}
			for (Jugador j : jugadores) {
				assertNotNull(j.getId());
				assertNotNull(j.getNombre());
				assertNotNull(j.getFechaNacimiento());
				assertNotNull(j.getEquipo());
			}

		} catch (Exception e) {
			fail("TEST NO SUPERADO: No se han insertado el jugador al producirse una excepcion: "
					+ e.getLocalizedMessage());
		}

	}

	@Test(expected = ServicioFutbolException.class)
	public void testNuevoJugadorSinEquipoIntroducidoFail() throws Exception {

		sf.nuevoJugador(3, "Messi", "2/3/1981", 2);

	}

	@Test(expected = ServicioFutbolException.class)
	public void testNuevoJugadorConFechaMalFail() throws Exception {
                sf.nuevoEquipo(1, "Real Madrid", 1000);
		sf.nuevoJugador(3, "Messi", "31-1-1981", 1);

	}

	@Test(expected = ServicioFutbolException.class)
	public void testNuevoJugadorConIdRepetidoFail() throws Exception {
                sf.nuevoEquipo(1, "Real Madrid", 1000);
                sf.nuevoJugador(1, "Gareth Bale", "2/3/1980", 1);
		sf.nuevoJugador(1, "Messi", "2/3/1981", 1);

	}

	@Test(expected = ServicioFutbolException.class)
	public void testNuevoJugadorIdNegativoFail() throws Exception {
                sf.nuevoEquipo(1, "Real Madrid", 1000);
		sf.nuevoJugador(-1, "Messi", "2/3/1981", 1);

	}

	@Test
	public void testNuevoEquipo() {
		try {
			sf.nuevoEquipo(2, "Barcelona", 50);
			List<Equipo> equipos = sf.obtenerTodosEquipos();
			if (equipos.size() != 1) {
				fail("No se ha insertado el equipo");
			}
			for (Equipo e : equipos) {
				assertNotNull(e.getId());
				assertNotNull(e.getNombre());
				assertNotNull(e.getPresupuesto());

			}

		} catch (Exception e) {
			fail("TEST NO SUPERADO: No se han insertado el equipo al producirse una excepcion: "
					+ e.getLocalizedMessage());
		}
	}

	@Test(expected = ServicioFutbolException.class)
	public void testNuevoEquipoConIdRepetidoFail() throws Exception {
                sf.nuevoEquipo(1, "Madrid", 100);
		sf.nuevoEquipo(1, "Barcelona", 50);

	}

	@Test(expected = ServicioFutbolException.class)
	public void testNuevoEquipoIdNegativoFail() throws Exception {

		sf.nuevoEquipo(-1, "Barcelona", 50);

	}

	@Test
	public void testObtenerTodosJugadores() {
		try {
                         sf.nuevoEquipo(1, "Real Madrid", 1000);
                        sf.nuevoJugador(1, "Gareth Bale", "2/3/1980", 1);

			List<Jugador> jugadores = sf.obtenerTodosJugadores();
			if (jugadores.size() != 1) {
				fail("No se han obtenido todos los equipos");
			}
			for (Jugador j : jugadores) {
				assertNotNull(j.getId());
				assertNotNull(j.getNombre());
				assertNotNull(j.getFechaNacimiento());
				assertNotNull(j.getEquipo());
			}

		} catch (Exception e) {
			fail("TEST NO SUPERADO: No se han obtenido los jugadores al producirse una excepcion: "
					+ e.getLocalizedMessage());
		}
	}

	@Test
	public void testObtenerTodosEquipos() {
		try {
                        sf.nuevoEquipo(1, "Real Madrid", 1000);
			List<Equipo> equipos = sf.obtenerTodosEquipos();
			if (equipos.size() != 1) {
				fail("No se han obtenido los equipos correctamente");
			}
			for (Equipo e : equipos) {
				assertNotNull(e.getId());
				assertNotNull(e.getNombre());
				assertNotNull(e.getPresupuesto());

			}

		} catch (Exception e) {
			fail("TEST NO SUPERADO: No se han obtenido los equipos al producirse una excepcion: "
					+ e.getLocalizedMessage());
		}
	}

	@Test
	public void testEliminarJugador() {
		try {
                        sf.nuevoEquipo(1, "Real Madrid", 1000);
                        sf.nuevoJugador(1, "Gareth Bale", "2/3/1980", 1);
                        sf.nuevoJugador(2, "Messi", "2/3/1981", 1);
                    
			sf.eliminarJugador(1);
			List<Jugador> jugadores = sf.obtenerTodosJugadores();
			if (jugadores.size() != 1) {
				fail("No se ha eliminado el jugador");
			}
			for (Jugador j : jugadores) {
				assertNotNull(j.getId());
				assertNotNull(j.getNombre());
				assertNotNull(j.getFechaNacimiento());
				assertNotNull(j.getEquipo());
			}

		} catch (Exception e) {
			fail("TEST NO SUPERADO: No se ha eliminado el jugador al producirse una excepcion: "
					+ e.getLocalizedMessage());
		}
	}

}
