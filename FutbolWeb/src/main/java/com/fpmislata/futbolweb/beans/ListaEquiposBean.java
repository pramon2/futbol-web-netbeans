/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fpmislata.futbolweb.beans;

import com.fpmislata.entidades.Equipo;
import com.fpmislata.negocio.FactoriaServicios;
import com.fpmislata.negocio.IServicioFutbol;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;


import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Pablo
 */
@ManagedBean
@SessionScoped
public class ListaEquiposBean implements Serializable {

    private List<Equipo> equipos=null;
  
    public ListaEquiposBean() {
        
         System.out.println("Inicializando  ListaEquiposBean");

    }

    public List<Equipo> getEquipos() {
        try {
            
            IServicioFutbol isf=FactoriaServicios.getInstance().getServicioFutbol();
            equipos = isf.obtenerTodosEquipos();
          
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                    .addMessage(null, new FacesMessage("Ha habido un errar al obtener los equipos"+e.getLocalizedMessage()));
            System.out.println("Error"+e.getLocalizedMessage());
        }
        
        return equipos;

    }

    public void setEquipos(List<Equipo> equipos) {
        this.equipos = equipos;
    }

}
