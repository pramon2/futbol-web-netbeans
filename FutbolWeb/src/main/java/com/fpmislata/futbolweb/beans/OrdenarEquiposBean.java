/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fpmislata.futbolweb.beans;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import org.richfaces.component.SortOrder;

/**
 *
 * @author Pablo
 */
@ManagedBean
@SessionScoped
public class OrdenarEquiposBean implements Serializable{
    /**
     *
     */
    private static final long serialVersionUID = -6237417487105926855L;
    
    private SortOrder idOrder = SortOrder.unsorted;
    private SortOrder nombreOrder = SortOrder.unsorted;
    private SortOrder presupuestoOrder = SortOrder.unsorted;
 
    public void ordenarPorId() {
        nombreOrder = SortOrder.unsorted;
        presupuestoOrder = SortOrder.unsorted;
        if (idOrder.equals(SortOrder.ascending)) {
            setIdOrder(SortOrder.descending);
        } else {
            setIdOrder(SortOrder.ascending);
        }
    }
     public void ordenarPorNombre() {
        idOrder = SortOrder.unsorted;
        presupuestoOrder = SortOrder.unsorted;
        if (nombreOrder.equals(SortOrder.ascending)) {
            setNombreOrder(SortOrder.descending);
        } else {
            setNombreOrder(SortOrder.ascending);
        }
    }
      public void ordenarPorPresupuesto() {
        nombreOrder = SortOrder.unsorted;
        idOrder = SortOrder.unsorted;
        if (presupuestoOrder.equals(SortOrder.ascending)) {
            setPresupuestoOrder(SortOrder.descending);
        } else {
            setPresupuestoOrder(SortOrder.ascending);
        }
    }

    public SortOrder getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(SortOrder idOrder) {
        this.idOrder = idOrder;
    }

    public SortOrder getNombreOrder() {
        return nombreOrder;
    }

    public void setNombreOrder(SortOrder nombreOrder) {
        this.nombreOrder = nombreOrder;
    }

    public SortOrder getPresupuestoOrder() {
        return presupuestoOrder;
    }

    public void setPresupuestoOrder(SortOrder presupuestoOrder) {
        this.presupuestoOrder = presupuestoOrder;
    }
    
 
    
 
    
}
