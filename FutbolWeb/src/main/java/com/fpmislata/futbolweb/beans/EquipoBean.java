/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fpmislata.futbolweb.beans;



import com.fpmislata.negocio.FactoriaServicios;
import com.fpmislata.negocio.IServicioFutbol;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Pablo
 */
@ManagedBean
@RequestScoped
public class EquipoBean implements Serializable {
    @NotNull(message="El equipo debe tener identificador")
    @Min(value=1,message="El identificador debe ser un número mayor que 0")
    private int id;
    
    @NotNull(message="El equipo debe tener nombre")
    @Size(min=2,message="El nombre del equipo debe tener al menos dos caracteres")
    private String nombre;
    @Min(value=1,message="El presupuesto debe ser de al menos un euro")
    private double presupuesto=0;
    
   

    
           
    public EquipoBean() {
        System.out.println("Inicializando  equipoBean");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPresupuesto() {
        return presupuesto;
    }

    public void setPresupuesto(double presupuesto) {
        this.presupuesto = presupuesto;
    }
    
    
    public String guardarEquipo(){
        FacesContext context = FacesContext.getCurrentInstance();
         try {
              
           
           IServicioFutbol isf=FactoriaServicios.getInstance().getServicioFutbol();
           isf.nuevoEquipo(id,nombre,presupuesto);
           context.getExternalContext().getFlash().put("introOk", "El equipo se ha introducido correctamente");
           return "to-verEquipos";

        } catch (Exception e) {
           
         
            context.addMessage(null, new FacesMessage("Ha habido un errar al crear el equipo"+e.getLocalizedMessage()));
            System.out.println("Error"+e.getLocalizedMessage());
            return "to-nuevoEquipo";
        }
        
    }
    
}
