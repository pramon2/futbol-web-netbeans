/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fpmislata.futbolweb.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Pablo
 */
@ManagedBean
@RequestScoped
public class JugadorBean {
    @NotNull(message="El jugador debe tener identificador")
    @Min(value=1,message="El identificador debe ser un número mayor que 0")
    private int id;
    
    @NotNull(message="El jugador debe tener nombre")
    @Size(min=2,message="El nombre del jugador debe tener al menos dos caracteres")
    private String nombre;
    
    private String fechaNac;
    
    @NotNull(message="El jugador debe jugar en algún equipo")
    @Min(value=1,message="El identificador del equipo debe ser un número mayor que 0")
    private int idEquipo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    public int getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(int idEquipo) {
        this.idEquipo = idEquipo;
    }
    
    
}
